export default function countTypesNumber(source) {
  // TODO 6: 在这里写实现代码
  const values = Object.values(source).map(item => +item);

  return values.reduce((acc, cur) => acc + cur);
}
